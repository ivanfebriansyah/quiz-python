# Create main menu function
def main_menu():
    # Creating menu
    print("## Daftar Menu Kopi Ilkom ##")
    print("=============================")
    print("1. Espresso")
    print("2. Cappucino")
    print("3. Moccacino")
    print("4. Vanilla Latte")
    print("5. Hazelnut Latte")
    print()
    # Input menu
    pilihan = input("Pilihan anda : ")

    # basic if else logic for menu
    if pilihan == '1':
        print("Pilihan anda : 1")
        print("Anda memilih Espresso")
        print()
        tanya()
    elif pilihan == '2':
        print("Pilihan anda : 2")
        print("Anda memilih Cappucino")
        print()
        tanya()
    elif pilihan == '3':
        print("Pilihan anda : 3")
        print("Anda memilih Moccacino")
        print()
        tanya()
    elif pilihan == '4':
        print("Pilihan anda : 4")
        print("Anda memilih Vanilla Latte")
        print()
        tanya()
    elif pilihan == '5':
        print("Pilihan anda : 5")
        print("Anda memilih Hazelnut Latte")
        print()
        tanya()
    else:
        print('Tidak ada menu yang dipilih ')
        tanya()


def tanya():
    tanya = input('Ingin memilih menu lain (y/n)? ')
    if tanya == 'y':
        main_menu()
    elif tanya == 't':
        exit()
    else:
        print('input salah')
        print('masukan input dengan benar')

main_menu()