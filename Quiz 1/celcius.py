# Celcius to Fahrenheit, Kelvin And Reamur


# Reading temperature in celcius
celcius = float(input('Enter temperature in celsius: '))

# Converting

fahrenheit = (9/5) * celcius + 32
kelvin  = celcius + 273.15
reamur = (4/5) * celcius

# Displaying output
print('%0.3f Celsius = %0.3f Fahrenheit.' % (celcius, fahrenheit))
print('%0.3f Celsius = %0.3f Kelvin.' % (celcius, kelvin))
print('%0.3f Celsius = %0.3f Reamur.' % (celcius, reamur))