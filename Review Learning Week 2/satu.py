# Bikin list tuples
string_tuples = [('red', 'pink'), ('white','black'), ('orange', 'green')]

# fungsi konversi tuple ke string
def gabungin(strings_tuple) -> str:
   return ' '.join(strings_tuple)

# menggabungkan tuple menggunakan func map
result = map(gabungin, string_tuples)

#List tuple asal
print("List tuple asal")
print("%s" % string_tuples)

# Convert dan print hasil
print("\nGabungan string tuple: ")
print(list(result))