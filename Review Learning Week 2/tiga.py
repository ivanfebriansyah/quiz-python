# fungsi mencari elemen yang sama
def carikembar(a, b):
    a_set = set(a)
    b_set = set(b)

    if (a_set & b_set):
        print("\nNilai yang sama : ")
        print(a_set & b_set)
        print("Total jumlah kembar : ")
        print(len(a_set & b_set))
    else:
        print("No common elements")

# input list
a = [1, 2, 3, 4, 5, 6, 7, 8]
b = [2, 2, 3, 1, 2, 6, 7, 9]

print("list asli : ")
print(a)
print(a)

# memanggil fungsi
carikembar(a, b)

