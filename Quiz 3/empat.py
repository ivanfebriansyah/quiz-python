# Membuat variabel list
num_list = []

# Membuat var n, k  untuk inputan
n = int(input("Masukan angka awal rentang : "))
k = int(input("Masukan angka akhir rentang : "))
a = n
# Menambahkan 1. n+1
b = k+1

#  Looping untuk
# menambahkan value ke variabel num_list
for i in range(a,b):
  num_list.append(i)

# Menampilkan list yang diinputkan
print("\nOriginal List:", num_list)
# membuat variabel list intuk penampung angka Genap
even_list=[]
# membuat variabel list intuk penampung angka Ganjil
odd_list=[]

# Logika sederhana untuk menentukan
# Ganjil genap
for i in range(len(num_list)):
    # Genap bagi dua habis
  if(num_list[i]%2==0):
    even_list.append(num_list[i])
  else:
    odd_list.append(num_list[i])

# Menampilkan output
print("\nBilangan Genap:")
print(even_list)
print("\nBilangan Ganjil:")
print(odd_list)