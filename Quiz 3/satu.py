# List calculation

# Membuat variabel penampung data
# Dengan tipe data list
lst = []

# Membuat variabel input
num = int(input('Berapa angka: '))

# Membuat looping input sesuai inputan
for n in range(num):
    numbers = int(input('Masukan angkanya: '))
    lst.append(numbers)

# Menampilkan inputan dalam variabel lst
print("\nMasukan : " , lst)

# Membuat fungsi kalkulasi
def kalkulasi(lst):
    return sum(lst)

# menjumlahkan menggunakan sum function
print("Hasil Penjumlahan : ", kalkulasi(lst))