# Membuat inputan sekaligus variabel
nama = input('Nama karyawan : ')
golongan = input("Upah golongan : ")
jam = int(input("Kamu kerja berapa jam : "))

# Logika HITUNGAN menggunakan if untuk karyawan yang lembur
if jam > 48:
    lembur = (jam - 48) * 4000
else:
    jam = jam

# Logika sederhana if jika sama dengan maka
if golongan == 'A':
    gaji = 5000 * jam
    print('\nNama : ', nama)
    print('Gaji : ', gaji)
elif golongan == 'B':
    gaji = 7000 * jam
    print('\nNama : ', nama)
    print('Gaji : ', gaji)
elif golongan == 'C':
    gaji = 8000 * jam
    print('\nNama : ', nama)
    print('Gaji : ', gaji)
elif golongan == 'D':
    gaji = 10000 * jam
    print('\nNama : ', nama)
    print('Gaji : ', gaji)
else:
    print('Inputan salah !!')
