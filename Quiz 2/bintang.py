# variabel string untuk menampung *
string = ""
# bar  = 1 karena dimulai dari satu
baris = 1

# Inputan mau berapa banya *
x = int(input("Masukan Jumlah Maks Bintang : "))

# Looping Baris
while baris <= x:
    kol = baris

    # Looping Kolom
    while kol > 0:
        string = string + " * "
        kol = kol - 1

    # baris baru setiap string
    string = string + "\n"
    baris = baris + 1

# print value
print(string)
